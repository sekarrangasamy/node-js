var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');


var app = express();

app.all('*',function(req,res,next){
	res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
})

app.set('port', (process.env.PORT || 3000));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/sekar", function(req, res){
    console.log("Post")
    return res.json({"welome":"sekar"});
});
app.post("/sekar/:userid", function(req, res){
    console.log("param", req.params.userid)
    res.status(200).send('created')   
});
app.put("/sekar/:userid", function(req, res){
    console.log("param", req.params.userid)
    res.status(201).send('updated')   
});
app.delete("/sekar/:userid", function(req, res){
    console.log("param", req.params.userid)
    res.status(200).send('deleted')
});


http.createServer(app).listen(app.get('port'), function(){
        console.log("server is running ...");
});